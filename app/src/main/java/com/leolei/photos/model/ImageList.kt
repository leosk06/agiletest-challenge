package com.leolei.photos.model

data class ImageList(
    val pictures: List<Photo>,
    val page: Int,
    val pageCount: Int,
    val hasMore: Boolean
)