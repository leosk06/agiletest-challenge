package com.leolei.photos.grid

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.leolei.photos.PHOTO_ID
import com.leolei.photos.detail.PhotoActivity
import com.leolei.photos.R
import com.leolei.photos.rest.PhotoRestClient
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GridActivity : AppCompatActivity() {
    @Inject
    lateinit var photoRestClient: PhotoRestClient

    private val recyclerView by lazy { findViewById<RecyclerView>(R.id.photos) }
    private val photoAdapter by lazy { PhotoAdapter(this) }

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid)

        val columnCount = resources.getInteger(R.integer.column_count)
        photoAdapter.onPhotoClicked = {
            val intent = Intent(this@GridActivity, PhotoActivity::class.java).apply {
                putExtra(PHOTO_ID, it.id)
            }
            startActivity(intent)
        }
        recyclerView.apply {
            adapter = photoAdapter
            layoutManager = GridLayoutManager(context, columnCount)
        }

        disposable.add(
            photoRestClient.getImageList()
                .subscribe(
                    { result ->
                        photoAdapter.data = result.pictures
                    },
                    { error ->
                        Toast.makeText(this@GridActivity, error.localizedMessage, Toast.LENGTH_LONG).show()
                        error.printStackTrace()
                    }
                )
        )
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}
