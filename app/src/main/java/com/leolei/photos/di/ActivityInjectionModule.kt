package com.leolei.photos.di

import com.leolei.photos.detail.PhotoActivity
import com.leolei.photos.grid.GridActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectionModule {
    @ContributesAndroidInjector
    abstract fun bindGridActivity(): GridActivity

    @ContributesAndroidInjector
    abstract fun bindPhotoActivity(): PhotoActivity
}
