package com.leolei.photos.grid

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.leolei.photos.R
import com.leolei.photos.model.Photo

class PhotoVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    lateinit var onPhotoClicked: (Photo) -> Unit
    lateinit var photo: Photo

    val photoView by lazy { itemView.findViewById<ImageView>(R.id.photo) }

    init {
        itemView.setOnClickListener { onPhotoClicked(photo) }
    }
}