package com.leolei.photos.di

import com.leolei.photos.rest.PhotoRestService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object AppModule {
    @Provides
    @Singleton
    @JvmStatic
    fun provideOkHttpClient(@ApiKey apiKey: String): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(2000, TimeUnit.MILLISECONDS)
            .callTimeout(2000, TimeUnit.MILLISECONDS)
            .readTimeout(2000, TimeUnit.MILLISECONDS)
            .writeTimeout(2000, TimeUnit.MILLISECONDS)
            .build() //TODO: Interceptor
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideRetrofitClient(@BaseUrl baseUrl: String, client: OkHttpClient): PhotoRestService {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(PhotoRestService::class.java)
    }
}
