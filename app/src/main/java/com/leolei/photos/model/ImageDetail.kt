package com.leolei.photos.model

data class ImageDetail(
    val author: String,
    val camera: String,
    val pictureUrl: String
)