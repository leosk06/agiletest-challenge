package com.leolei.photos.rest.model

import com.google.gson.annotations.SerializedName

data class ImageListResponse(
    val pictures: List<Photo>,
    val page: Int,
    val pageCount: Int,
    val hasMore: Boolean
)

data class Photo(
    @SerializedName("id")
    val id: String,

    @SerializedName("cropped_picture")
    val url: String
)