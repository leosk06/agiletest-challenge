How to build and run
--------------------

From Android Studio:
- Press the Run button and choose a device or emulator (API 16+ supported)
    - On Mac (and probably also on Windows and Linux), you can press Ctrl+R instead.

From the command line:
- first make sure there is a device or an emulator connected
- cd to the repo root
- ./gradlew installDebug

Miscellaneous
- One Activity is written in Kotlin and the other one in Java just to make the point I can write both.
- This project didn't take 2-3 hours as specified. To see what I did in 2-3 hours, you can check the repo history.
