package com.leolei.photos.rest.model

data class AuthBody(
    val apiKey: String
)
