package com.leolei.photos.grid

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.leolei.photos.R
import com.leolei.photos.model.Photo
import com.squareup.picasso.Picasso

class PhotoAdapter(
    context: Context
) : RecyclerView.Adapter<PhotoVH>() {
    lateinit var onPhotoClicked: (Photo) -> Unit

    var data: List<Photo>
        get() = items
        set(value) {
            items.clear()
            items.addAll(value)
            notifyDataSetChanged()
        }

    private val items = ArrayList<Photo>()

    private val inflater = LayoutInflater.from(context)
    private val picasso = Picasso.with(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoVH {
        val view = inflater.inflate(R.layout.item_photo, parent, false)
        val holder = PhotoVH(view)
        holder.onPhotoClicked = onPhotoClicked
        return holder;
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: PhotoVH, position: Int) {
        picasso.cancelRequest(holder.photoView)

        val item = items[position]
        holder.photo = item
        picasso.load(item.url).into(holder.photoView)
    }
}