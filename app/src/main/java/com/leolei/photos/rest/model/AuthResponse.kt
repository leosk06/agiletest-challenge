package com.leolei.photos.rest.model

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("auth")
    val isAuthorized: Boolean,

    @SerializedName("token")
    val bearerToken: String
)