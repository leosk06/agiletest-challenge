package com.leolei.photos.di

import com.leolei.photos.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
annotation class BaseUrl

@Qualifier
annotation class ApiKey

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ActivityInjectionModule::class,
    AppModule::class
])
interface AppComponent {
    fun inject(app: App)

    @BaseUrl
    fun baseUrl(): String

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: App): Builder

        @BindsInstance
        fun baseUrl(@BaseUrl baseUrl: String): Builder

        @BindsInstance
        fun apiKey(@ApiKey apiKey: String): Builder

        fun build(): AppComponent
    }
}