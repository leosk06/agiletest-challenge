package com.leolei.photos.model

data class Photo(
    val id: String,
    val url: String
)