package com.leolei.photos.rest

import com.leolei.photos.di.ApiKey
import com.leolei.photos.model.ImageDetail
import com.leolei.photos.model.ImageList
import com.leolei.photos.rest.model.AuthBody
import com.leolei.photos.rest.model.AuthResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject
import com.leolei.photos.model.Photo as ModelPhoto

class PhotoRestClient @Inject constructor(
    private val rawRestClient: PhotoRestService,
    @ApiKey
    private val apiKey: String
) {
    private val authBody = AuthBody(apiKey)
    private var bearerToken: String? = null

    fun getImageList(pageNumber: Int = 1, limit: Int = 20): Observable<ImageList> {
        return tokenAwareRequest {
            rawRestClient.getImageList(authHeader(), pageNumber, limit)
                .applySchedulers()
                .map {
                    ImageList(
                        pictures = it.pictures.map { ModelPhoto(it.id, it.url) },
                        page = it.page,
                        pageCount = it.pageCount,
                        hasMore = it.hasMore
                    )
                }
        }
    }

    fun getSpecificImage(photoId: String): Observable<ImageDetail> {
        return tokenAwareRequest {
            rawRestClient.getSpecificImage(authHeader(), photoId)
                .applySchedulers()
                .map {
                    ImageDetail(
                        it.author,
                        it.camera,
                        it.pictureUrl
                    )
                }
        }
    }

    private fun <T> tokenAwareRequest(closure: () -> Observable<T>): Observable<T> {
        return closure()
            .onErrorResumeNext { throwable: Throwable ->
                if (throwable is HttpException && throwable.code() == CODE_UNAUTHORIZED) {
                    getBearerToken().switchMap { closure() }
                } else {
                    throw throwable
                }
            }
    }
    
    private fun getBearerToken(): Observable<AuthResponse> {
        return rawRestClient.getBearerToken(authBody)
            .applySchedulers()
            .doOnNext { bearerToken = it.bearerToken }
    }

    private fun authHeader() = if (bearerToken == null) null else "Bearer $bearerToken"

    companion object {
        private val CODE_UNAUTHORIZED = 401
    }
}

private fun <T> Observable<T>.applySchedulers(): Observable<T> {
    return this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}