package com.leolei.photos.rest

import com.leolei.photos.rest.model.AuthBody
import com.leolei.photos.rest.model.AuthResponse
import com.leolei.photos.rest.model.ImageListResponse
import io.reactivex.Observable
import retrofit2.http.*

interface PhotoRestService {
    @POST("/auth")
    fun getBearerToken(@Body authBody: AuthBody): Observable<AuthResponse>

    @GET("/images")
    fun getImageList(
        @Header("Authorization") authHeader: String?,
        @Query("page") pageNumber: Int,
        @Query("limit") limit: Int
    ): Observable<ImageListResponse>

    @GET("/images/{photoId}")
    fun getSpecificImage(
        @Header("Authorization") authHeader: String?,
        @Path("photoId") photoId: String
    ): Observable<SinglePhotoResponse>
}