package com.leolei.photos.rest

import com.google.gson.annotations.SerializedName

data class SinglePhotoResponse(
    @SerializedName("author")
    val author: String,

    @SerializedName("camera")
    val camera: String,

    @SerializedName("full_picture")
    val pictureUrl: String
)
