package com.leolei.photos.detail;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.leolei.photos.R;
import com.leolei.photos.model.ImageDetail;
import com.leolei.photos.rest.PhotoRestClient;
import com.squareup.picasso.Picasso;
import dagger.android.AndroidInjection;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

import javax.inject.Inject;

import java.util.List;

import static com.leolei.photos.ConstantsKt.PHOTO_ID;

public class PhotoActivity extends AppCompatActivity {
    private static final String PHOTO_URL = "purl";

    @Inject
    PhotoRestClient photoRestClient;

    private CompositeDisposable disposable = new CompositeDisposable();

    private TextView author;
    private TextView camera;
    private ImageView photo;

    private final Picasso picasso = Picasso.with(this);
    private String photoUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        if (savedInstanceState != null) {
            photoUrl = savedInstanceState.getString(PHOTO_URL);
        }

        author = findViewById(R.id.author);
        camera = findViewById(R.id.camera);
        photo = findViewById(R.id.photo);

        setupFab();
        fetchPhoto();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PHOTO_URL, photoUrl);
    }

    private void setupFab() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePhoto();
            }
        });
    }

    private void fetchPhoto() {
        final String photoId = getIntent().getStringExtra(PHOTO_ID);
        disposable.add(
                photoRestClient.getSpecificImage(photoId)
                        .subscribe(
                                new Consumer<ImageDetail>() {

                                    @Override
                                    public void accept(ImageDetail result) throws Exception {
                                        author.setText(result.getAuthor());
                                        camera.setText(result.getCamera());

                                        photoUrl = result.getPictureUrl();
                                        picasso
                                                .load(result.getPictureUrl())
                                                .into(photo);
                                    }
                                },
                                new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Toast.makeText(PhotoActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                        )
        );
    }

    private void sharePhoto() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(
                Intent.EXTRA_TEXT,
                getString(
                        R.string.share_message,
                        photoUrl
                )
        );

        final List<ResolveInfo> info = getPackageManager().queryIntentActivities(
                shareIntent, 0
        );
        if (!info.isEmpty()) {
            startActivity(shareIntent);
        } else {
            Toast.makeText(PhotoActivity.this, R.string.no_app_for_sharing, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposable.clear();
        picasso.cancelRequest(photo);
    }
}
